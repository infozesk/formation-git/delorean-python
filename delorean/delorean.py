#!/usr/bin/env python
# encoding: utf-8

" A delorean module in python"


from datetime import date, datetime, timedelta
import time
import random

__version__ = "1.0.0"


class Delorean(object):
    """A delorean is a time traveling car"""

    def __init__(self):
        super(Delorean, self).__init__()
        self.__speed = 0
        self.plutonium = False
        self.date = datetime.now().date()
        self.__target_date = None

    @property
    def speed(self):
        """return the delorean speed
        
        :rtype: int"""
        return self.__speed

    @speed.setter
    def speed(self, speed):
        """set the delorean speed
        
        :param speed: The speed to set
        :type speed: int
        """
        self.__speed = speed
        self.__speed_changed()

    def insert_plutonium(self):
        """Insert plutonium in the reactor"""
        self.plutonium = True

    @property
    def has_plutonium(self):
        """return True if there is plutonium
        
        :rtype: bool
        """
        return self.plutonium

    @property
    def target_date(self):
        "return the current delorean target date"
        return self.__target_date

    @target_date.setter
    def target_date(self, t_date):
        """Define the delorean target date, date shall follow format DD/MM/YYYY

        If t_date is None, unset target date
        :param t_date: The target date under %d/%m/%Y format, such as 10/06/1980
        :type t_date: str 
        """
        if t_date is None:
            self.__target_date = None
            return
        self.__target_date = datetime.strptime(t_date, "%d/%m/%Y").date()

    def __speed_changed(self):
        "A callback to be called when speed change"
        if self.target_date and self.has_plutonium and self.speed > 88:
            self.date = self.target_date
            self.plutonium = False
            self.target_date = None
            print("Delorean traveled in time to %s" % self.date.strftime("%d/%m/%Y"))


def main():
    print("======== DELOREAN %s ============" % __version__)
    print("Doc construit une delorean")
    delo = Delorean()
    print("Nous sommes le %s" % delo.date)

    print("Doc refait le plein de plutonium")
    delo.insert_plutonium()

    print("Doc programme un voyage dans le temps pour retourner en 1955")
    delo.target_date = "05/11/1955"

    print("Doc accelere jusqu a 89mph")
    delo.speed = 89

    print("####### VOYAGE DANS LE TEMPS #########")
    print("Nous sommes le %s" % delo.date)


if __name__ == "__main__":
    main()
